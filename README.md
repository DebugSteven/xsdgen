# `xsdgen`

This crate parses an XSD definition file and creates Rust types that correspond
to the types defined in it, with all the corresponding relationships preserved.

The generated code provides the ability to parse an XML document into the
corresponding tree, or to build up a tree programmatically.

## Project Structure

This is primarily a library crate, and provides a single command-line driver in
`src/main.rs`. All business logic used in the driver executable must be provided
and exposed by the library. The driver itself must solely be a bridge between
the operating system and the library.

## Usage

Code generation can be done either with the command-line driver or it can be
incorporated into your project via the pre-compile build script, typically
`build.rs`.
